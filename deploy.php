<?php

require 'recipe/symfony3.php';

// Import the server configuration
serverList('app/config/servers.yml');

// Configure the project repository
set('repository', 'git@gitlab.com:hausdesign/intranet.git');

// Run the database migrations before warming the cache
before('deploy:symlink', 'database:migrate');
